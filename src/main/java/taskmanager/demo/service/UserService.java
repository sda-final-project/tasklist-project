package taskmanager.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import taskmanager.demo.dto.UserDTO;
import taskmanager.demo.enums.RoleEnum;
import taskmanager.demo.model.User;
import taskmanager.demo.repository.RoleRepository;
import taskmanager.demo.repository.UsersRepository;

@Service
public class UserService {
    private UsersRepository usersRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserService(UsersRepository usersRepository, RoleRepository roleRepository) {
        this.usersRepository = usersRepository;
        this.roleRepository = roleRepository;
    }

    public User addUser(UserDTO userDTO) {
        User user = new User();
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setUserName(userDTO.getUserName());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setQuestion(userDTO.getQuestion());
        user.setAnswer(userDTO.getAnswer());
        user.setLockalBirth(userDTO.getLockalBirth());
        user.setRole(roleRepository.findByName(RoleEnum.ROLE_USER.getRoleName()));

        User newUser = usersRepository.save(user);
        return newUser;
    }
}
