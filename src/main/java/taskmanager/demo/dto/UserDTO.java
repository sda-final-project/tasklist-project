package taskmanager.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class UserDTO {
    private String userName;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private String question;
    private String answer;
    private String lockalBirth;
}
