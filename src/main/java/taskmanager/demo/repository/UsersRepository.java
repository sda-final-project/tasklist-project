package taskmanager.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import taskmanager.demo.model.User;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {

    User findByUserName(String username);
}
