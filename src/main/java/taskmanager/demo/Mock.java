package taskmanager.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import taskmanager.demo.enums.RoleEnum;
import taskmanager.demo.model.Role;
import taskmanager.demo.model.User;
import taskmanager.demo.repository.RoleRepository;
import taskmanager.demo.repository.UsersRepository;

import javax.annotation.PostConstruct;

@Component
public class Mock {

    private RoleRepository roleRepository;
    private UsersRepository usersRepository;

    @Autowired
    public Mock(RoleRepository roleRepository, UsersRepository usersRepository) {
        this.roleRepository = roleRepository;
        this.usersRepository = usersRepository;
    }

    // utworzyć dwie role - ROLE_USER & ROLE_ADMIN oraz uzytkownikow admin & user
    @PostConstruct
    public void mockData() {

        Role roleAdmin = roleRepository.findByName(RoleEnum.ROLE_ADMIN.getRoleName());
        if (roleAdmin == null) {
            roleAdmin = new Role();
            roleAdmin.setName(RoleEnum.ROLE_ADMIN.getRoleName());
            roleRepository.save(roleAdmin);
        }
        Role roleUser = roleRepository.findByName(RoleEnum.ROLE_USER.getRoleName());
        if (roleUser == null) {
            roleUser = new Role();
            roleUser.setName(RoleEnum.ROLE_USER.getRoleName());
            roleRepository.save(roleUser);
        }
        

        User admin = usersRepository.findByUserName("admin");
        if (admin == null){
            admin = new User();
            admin.setUserName("admin");
            admin.setFirstName("Janek");
            admin.setLastName("Kowalski");
            admin.setPassword("1234");
            admin.setRole(roleAdmin);
            usersRepository.save(admin);
        }
        User user = usersRepository.findByUserName("user");
        if (user == null){
            user = new User();
            user.setUserName("user");
            user.setFirstName("Janek");
            user.setLastName("Kowalski");
            user.setPassword("1234");
            user.setRole(roleUser);
            usersRepository.save(user);
        }
    }
}
