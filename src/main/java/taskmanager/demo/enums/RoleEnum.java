package taskmanager.demo.enums;

import lombok.Getter;

@Getter
public enum RoleEnum {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_USER("ROLE_USER");

    private String roleName;

    RoleEnum(String roleName) {
        this.roleName = roleName;
    }
}
