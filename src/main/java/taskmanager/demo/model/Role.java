package taskmanager.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter

public class Role extends BasicEntity {

    @Column
    private String name;

    @OneToMany(mappedBy = "role")
    private List<User> listUser;

}
